name := "molecule-resolver-service"
version := "LATEST-SNAPSHOT"
scalaVersion := "2.11.8"
organization := "com.wiley.wss.applications"

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8",
  "-Ywarn-unused-import",
  "-Ywarn-unused",
  "-Ywarn-dead-code",
  "-Xfatal-warnings",
  "-Xlint:missing-interpolator"
)

libraryDependencies ++= {
  val actorVersion = "2.4.11"
  Seq(
    "com.typesafe.akka"  %% "akka-actor" % actorVersion,
    "com.typesafe.akka"  %% "akka-http-core" % actorVersion,
    "com.typesafe.akka"  %% "akka-http-experimental" % actorVersion,
    "com.typesafe.akka"  %% "akka-slf4j" % actorVersion,
    "de.heikoseeberger"  %% "akka-http-json4s" % "1.7.0",
    "org.json4s"         %% "json4s-jackson" % "3.4.0",
    "com.typesafe.slick" %% "slick" % "3.1.1",
    "com.typesafe.slick" %% "slick-hikaricp" % "3.1.1",
    "org.postgresql" % "postgresql" % "9.4.1209",
    "org.flywaydb"   % "flyway-core" % "4.0.3",
    "ch.qos.logback" % "logback-classic" % "1.1.7"
  )
}

Revolver.settings

lazy val root = (project in file("."))
  .enablePlugins(JavaAppPackaging)
  .enablePlugins(DockerPlugin)

dockerExposedPorts := Seq(8080)
dockerEntrypoint := Seq("bin/%s" format executableScriptName.value)
