package com.wiley.wss.resolver.configs

import org.flywaydb.core.Flyway

class Migration (implicit val config: Configuration) extends Flyway {

  setDataSource(config.jdbcUrl, config.dbUser, config.dbPassword)

  def migrateDatabase(): Migration = {
    super.migrate()
    this
  }

  def dropDatabase(): Migration = {
    super.clean()
    this
  }

  def reloadSchema(): Migration = {
    dropDatabase()
    migrate()
    this
  }
}