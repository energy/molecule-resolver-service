package com.wiley.wss.resolver.configs


import slick.driver.PostgresDriver

class Database(implicit val config: Configuration) {

  val driver = PostgresDriver

  val db: driver.backend.DatabaseDef = driver.api.Database.forConfig("database", config = config.config)

}