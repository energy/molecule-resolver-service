package com.wiley.wss.resolver.configs

import java.nio.file.{Files, Paths}

import com.typesafe.config.{ConfigFactory, ConfigRenderOptions}

class Configuration {
  private[resolver] val config = ConfigFactory.load()

  private val configData = config.root().render(ConfigRenderOptions.defaults().setOriginComments(false)).getBytes()
  Files.write(Paths.get("target", "config.conf"), configData)


  private val httpConfig = config.getConfig("http")
  private val databaseConfig = config.getConfig("database")

  val httpHost = httpConfig.getString("host")
  val httpPort = httpConfig.getInt("port")

  val jdbcUrl = databaseConfig.getString("url")
  val dbUser = databaseConfig.getString("properties.user")
  val dbPassword = databaseConfig.getString("properties.password")

}