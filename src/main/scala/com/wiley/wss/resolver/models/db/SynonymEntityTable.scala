package com.wiley.wss.resolver.models.db

import com.wiley.wss.resolver.configs.Database
import com.wiley.wss.resolver.models.SynonymEntity

trait SynonymEntityTable extends MoleculeEntityTable {

  protected val database: Database
  import database.driver.api._

  class Synonyms(tag: Tag) extends Table[SynonymEntity](tag, "synonyms") {
    def id         = column[Option[Long]]("id", O.PrimaryKey, O.AutoInc)
    def moleculeId = column[Option[Long]]("molecule_id")
    def name       = column[String]("name")

    def moleculeFk = foreignKey("molecule_fk", moleculeId, molecules)(_.id, onUpdate = ForeignKeyAction.Restrict, onDelete = ForeignKeyAction.Cascade)

    def * = (id, moleculeId, name) <> ((SynonymEntity.apply _).tupled, SynonymEntity.unapply)
  }

  protected val synonyms: TableQuery[Synonyms] = TableQuery[Synonyms]

}
