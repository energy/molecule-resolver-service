package com.wiley.wss.resolver.models.db

import com.wiley.wss.resolver.configs.Database
import com.wiley.wss.resolver.models.MoleculeEntity

trait MoleculeEntityTable {

  protected val database: Database
  import database.driver.api._

  class Molecules(tag: Tag) extends Table[MoleculeEntity](tag, "molecules") {
    def id        = column[Option[Long]]("id", O.PrimaryKey, O.AutoInc)
    def structure = column[String]("structure")
    def formula   = column[String]("formula")
    def smiles    = column[String]("smiles")
    def inchi     = column[String]("inchi")
    def inchiKey  = column[String]("inchikey")
    def weight    = column[Double]("weight")

    override def * = (id, structure, formula, smiles, inchi, inchiKey, weight) <>
      ((MoleculeEntity.apply _).tupled, MoleculeEntity.unapply)
  }

  protected val molecules: TableQuery[Molecules] = TableQuery[Molecules]
}
