package com.wiley.wss.resolver.models

case class MoleculeEntity
(
  id: Option[Long] = None,
  structure: String,
  formula: String,
  smiles: String,
  inchi: String,
  inchiKey: String,
  weight: Double) {

  require(!structure.isEmpty, "structure.empty")
}

/*case class Molecule(
   inchi: String,
   formula: String,
   inchiKey: String,
   weight: Double,
   nsc: String,
   release: String,
   atomStereoCenters: Int,
   bondStereoCenters: Int,
   fullAtomAndBondStereoSpecification: Int,
   cas: String,
   structureEvaluation: String,
   ncicaddFicusId: Seq[String],
   ncicaddUuuuuId: String,
   gusarPrediction: String,
   gusarPredictionAd: String,
   data: String
)*/