package com.wiley.wss.resolver.models

case class SynonymEntity(id: Option[Long] = None, moleculeId: Option[Long] = None, name: String) {
  require(!name.isEmpty, "name.empty")
}
