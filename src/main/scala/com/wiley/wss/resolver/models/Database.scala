package com.wiley.wss.resolver.models

case class Database(molecules: Int, synonyms: Int)