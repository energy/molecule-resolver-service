package com.wiley.wss.resolver.models

import akka.http.scaladsl.model.HttpCharset
import akka.http.scaladsl.model.MediaType._


object MimeTypes {

  private val utf8: HttpCharset = HttpCharset.custom("UTF-8")

  val `chemical/x-mdl-molfile`     = customWithFixedCharset("chemical", "x-mdl-molfile",     utf8, List("mol"))
  val `chemical/x-daylight-smiles` = customWithFixedCharset("chemical", "x-daylight-smiles", utf8, List("smi"))
  val `chemical/x-inchi`           = customWithFixedCharset("chemical", "x-inchi",           utf8, List("inchi"))

}

/*
todo:
chemical/x-alchemy 	alc 	Alchemy format 	example.alc 	example_alc 	http://www.camsoft.com/plugins/
chemical/x-cache-csf 	csf 	  	example.csf 	example_csf
chemical/x-cactvs-binary 	cbin 	CACTVS binary format 	example.cbin 	  	http://cactvs.cit.nih.gov
chemical/x-cactvs-binary 	cascii 	CACTVS ascii format 	example.cascii 	example_cascii 	http://cactvs.cit.nih.gov
chemical/x-cactvs-binary 	ctab 	CACTVS table format 	example.ctab 	  	http://cactvs.cit.nih.gov
chemical/x-cdx 	cdx 	ChemDraw eXchange file 	example.cdx 	  	http://www.camsoft.com/plugins/
chemical/x-cerius 	cer 	MSI Cerius II format 	example.cer 	example_cer 	http://www.msi.com/
chemical/x-chemdraw 	chm 	ChemDraw file 	example.chm 	  	http://www.camsoft.com/plugins/
chemical/x-cif 	cif 	Crystallographic Interchange Format 	example.cif 	example_cif 	http://www.bernstein-plus-sons.com/software/rasmol/
http://ndbserver.rutgers.edu/NDB/mmcif/examples/index.html
chemical/x-mmcif 	mcif 	MacroMolecular CIF 	example.mcif 	example_mcif 	http://www.bernstein-plus-sons.com/software/rasmol/
http://ndbserver.rutgers.edu/NDB/mmcif/examples/index.html
chemical/x-chem3d 	c3d 	Chem3D Format 	example.c3d 	  	CambridgeSoft
chemical/x-cmdf 	cmdf 	CrystalMaker Data format 	example.cmdf 	example_cmdf 	http://www.crystalmaker.co.uk/
chemical/x-compass 	cpa 	Compass program of the Takahashi 	example.cpa
chemical/x-crossfire 	bsd 	Crossfire file 	example.bsd 	example_bsd
chemical/x-cml 	cml 	Chemical Markup Language 	example.cml 	  	http://cml.sourceforge.net/
chemical/x-csml 	csml, csm 	Chemical Style Markup Language 	example.csml 	  	http://www.mdli.com/
chemical/x-ctx 	ctx 	Gasteiger group CTX file format 	example.ctx 	example_ctx
chemical/x-cxf 	cxf 	  	example.cxf
chemical/x-daylight-smiles 	smi 	Smiles Format 	example.smi 	example_smi 	http://www.daylight/dayhtml/smiles/index.html
chemical/x-embl-dl-nucleotide 	emb 	EMBL nucleotide format 	example.emb 	example_emb 	http://mercury.ebi.ac.uk/
chemical/x-galactic-spc 	spc 	SPC format for spectral and chromatographic data. 	example.spc 	  	http://www.galactic.com/galactic/
Data/spcvue.htm
chemical/x-gamess-input 	inp, gam 	GAMESS Input format 	example.inp 	example_inp 	http://www.msg.ameslab.gov/GAMESS/Graphics/MacMolPlt.shtml
chemical/x-gaussian-input 	gau 	Gaussian Input format 	example.gau 	example_gau 	http://www.mdli.com/
chemical/x-gaussian-checkpoint 	fch,fchk 	Gaussian Checkpoint format 	example.fch 	  	http://products.camsoft.com/
chemical/x-gaussian-cube 	cub 	Gaussian Cube (Wavefunction) format 	example.cub 	  	http://www.mdli.com/
chemical/x-gcg8-sequence 	gcg 	  	example.gcg 	example_gcg
chemical/x-genbank 	gen 	ToGenBank format 	example.gen 	example_gen 	http://www2.ebi.ac.uk:80/egcg-html/
chemical/x-isostar 	istr, ist 	IsoStar Library of intermolecular interactions 	example.istr 	example_istr 	http://www.ccdc.cam.ac.uk/
chemical/x-jcamp-dx 	jdx, dx 	JCAMP Spectroscopic Data Exchange Format 	example.dx 	example_dx 	http://www.mdli.com/
chemical/x-kinemage 	kin 	Kinetic (Protein Structure) Images 	example.kin 	example_kin 	http://www.faseb.org/protein/kinemages/
MageSoftware.html
chemical/x-macmolecule 	mcm 	MacMolecule File Format 	example.mcm 	example_mcm 	http://www.molvent.com/
chemical/x-macromodel-input 	mmd, mmod 	MacroModel Molecular Mechanics 	example.mmd 	example_mmd 	http://www.columbia.edu/cu/chemistry/
mmod/mmod.html
chemical/x-mdl-molfile 	mol 	MDL Molfile 	example.mol 	example_mol 	http://www.mdli.com/
chemical/x-mdl-rdfile 	rd 	Reaction-data file 	example.rd 	example_rd 	http://www.mdli.com/
chemical/x-mdl-rxnfile 	rxn 	MDL Reaction format 	example.rxn 	example_rxn 	http://www.mdli.com/
chemical/x-mdl-sdfile 	sd 	MDL Structure-data file 	example.sd 	example_sd 	http://www.mdli.com/
chemical/x-mdl-tgf 	tgf 	MDL Transportable Graphics Format 	example.tgf 	example_tgf 	http://www.mdli.com/
chemical/x-mif 	mif 	  	example.mif
chemical/x-mol2 	mol2 	Portable representation of a SYBYL molecule 	example.mol2 	example_mol2 	http://www.tripos.com/TechBriefs/mol2_format/mol2.html
chemical/x-molconn-Z 	b 	Molconn-Z format 	example.b 	example_b 	http://www.eslc.vabiotech.com/molconn/molconnz.html
chemical/x-mopac-input 	mop 	MOPAC Input format 	example.mop 	example_mop 	http://www.mdli.com/
chemical/x-mopac-graph 	gpt 	MOPAC Graph format 	example.gpt 	  	http://products.camsoft.com/
chemical/x-ncbi-asn1 	asn (old form) 	  	example.asn
chemical/x-ncbi-asn1-binary 	val 	  	example.val
chemical/x-pdb 	pdb 	Protein DataBank 	example.pdb 	example_pdb 	http://www.mdli.com/
chemical/x-swissprot 	sw 	SWISS-PROT protein sequence database 	example.sw 	example_sw 	http://www.expasy.ch/spdbv/text/download.htm
chemical/x-vamas-iso14976 	vms 	Versailles Agreement on Materials and Standards 	example.vms 	example_vms 	http://www.acolyte co uk/JISO/
chemical/x-vmd 	vmd 	Visual Molecular Dynamics 	example.vmd 	  	http://www.ks.uiuc.edu/Research/vmd/
chemical/x-xtel 	xtel 	Xtelplot file format 	example.xtel 	example_xtel 	http://www.iumsc.indiana.edu/graphics/xtelplot/xtelplot.htm
chemical/x-xyz 	xyz 	Co-ordinate Animation format 	example.xyz 	example_xyz 	http://www.mdli.com/
*/