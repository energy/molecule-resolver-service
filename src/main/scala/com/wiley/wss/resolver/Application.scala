package com.wiley.wss.resolver

import akka.actor.ActorSystem
import akka.event.Logging
import akka.stream.ActorMaterializer
import com.wiley.wss.resolver.configs.{Configuration, Database, Migration}
import com.wiley.wss.resolver.http.HttpService

object Application extends App {

  implicit val config = new Configuration()
  implicit val system = ActorSystem("resolver", config.config)
  implicit val materializer = ActorMaterializer()
  implicit val log = Logging(system, getClass)
  implicit val executor = system.dispatcher
  implicit val migration = new Migration()
  implicit val database = new Database()

  sys.addShutdownHook(system.terminate())

  migration.migrate()

  new HttpService()
}