package com.wiley.wss.resolver.http.api

import akka.http.scaladsl.model.{HttpResponse, StatusCodes}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server._
import akka.stream.ActorMaterializer
import com.wiley.wss.resolver.configs.{Configuration, Migration}
import com.wiley.wss.resolver.services.MoleculesService

import scala.concurrent.ExecutionContext

class DatabaseApi(val moleculesService: MoleculesService, migration: Migration)
                 (implicit executionContext: ExecutionContext,
                           config: Configuration,
                           materializer: ActorMaterializer) extends Serializer {

  val route: Route =
    pathPrefix("database") {
      get {
        val database = moleculesService.getDatabase()
        complete(database)
      } ~
      delete {
        migration.reloadSchema()
        complete(HttpResponse(status = StatusCodes.OK))
      }
    }
}
