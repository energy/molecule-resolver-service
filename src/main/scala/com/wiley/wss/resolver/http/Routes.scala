package com.wiley.wss.resolver.http

import akka.actor.ActorSystem
import akka.event.LoggingAdapter
import akka.http.scaladsl.model.headers.`Access-Control-Allow-Origin`
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import com.wiley.wss.resolver.configs.{Configuration, Database, Migration}
import com.wiley.wss.resolver.http.api.{DatabaseApi, MoleculesApi, UploadApi}
import com.wiley.wss.resolver.services.MoleculesService

import scala.concurrent.ExecutionContext

class Routes(implicit executionContext: ExecutionContext,
               materializer: ActorMaterializer,
               database: Database,
               migration: Migration,
               config: Configuration,
               system: ActorSystem,
               log: LoggingAdapter) {

  val moleculesService = new MoleculesService()

  private val uploadApi   = new UploadApi(moleculesService)
  private val moleculeApi = new MoleculesApi(moleculesService)
  private val databaseApi = new DatabaseApi(moleculesService, migration)

  val routes = respondWithDefaultHeader(`Access-Control-Allow-Origin`.`*`) {
    moleculeApi.route ~ uploadApi.route ~ databaseApi.route
  }
}