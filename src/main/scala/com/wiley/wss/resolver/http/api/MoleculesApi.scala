package com.wiley.wss.resolver.http.api

import akka.http.scaladsl.model.MediaType.WithFixedCharset
import akka.http.scaladsl.model.{ContentType, HttpEntity, _}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server._
import akka.stream.ActorMaterializer
import com.wiley.wss.resolver.configs.Configuration
import com.wiley.wss.resolver.models.MimeTypes._
import com.wiley.wss.resolver.models.MoleculeEntity
import com.wiley.wss.resolver.services.{FallbackProvider, MoleculesService}

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}


class MoleculesApi(val moleculesService: MoleculesService)
                  (implicit executionContext: ExecutionContext,
                            config: Configuration,
                            materializer: ActorMaterializer) extends Serializer {

  case class MoleculeResponse(molecule: MoleculeEntity, synonyms: Array[String])

  val molContentType = MediaTypes.`text/plain` withCharset HttpCharsets.`UTF-8`
  val fallbackProvider = new FallbackProvider()

  val route: Route =
    get {
      (path("fallback") & parameter("identifier")) { (identifier) =>
        fallbackProvider.fetchMolecule(identifier = identifier) match {
          case (molecule, synonyms) =>
            moleculesService.addMolecule(molecule, synonyms)
            complete((molecule, synonyms))
        }
      } ~
      (path("search") & parameter("query", 'limit.as[Int].?(15))) { (query, limit) =>
        val result = Await.result(moleculesService.findSynonyms(query, limit), Duration.Inf)
        complete(result)
      } ~
      (path("molecule" / Segment) & extractLog) {(filename, log) =>
        val extIndex: Int = filename.lastIndexOf(".")
        if (extIndex <= 0) {
          complete(unsupported(filename))
        } else {
          val (name, format) = filename.splitAt(extIndex)
          Await.result(moleculesService.findExactMolecule(name), Duration.Inf) match {
            case Some(x) =>
              val molecule = x._1
              complete(getMoleculeResponse(molecule, format.drop(1).toLowerCase()))
            case None =>
              if (config.config.getBoolean("fallback.enabled")) {
                fallbackProvider.fetchMolecule(identifier = name) match {
                  case (molecule, synonyms) =>
                    moleculesService.addMolecule(molecule, synonyms)
                    complete((molecule, synonyms))
                }
              } else {
                complete(HttpResponse(status = StatusCodes.NotFound, entity = HttpEntity(s"molecule not found: $name")))
              }
          }
        }
      }
    }

  def getMoleculeResponse(molecule: MoleculeEntity, format: String): AnyRef = {
    format match {
      case "json" =>
        val synonyms = Await.result(moleculesService.findSynonyms(molecule), Duration.Inf)
        MoleculeResponse(molecule, synonyms.map(_.name).toArray)
      case "inchikey" => ok(`chemical/x-inchi`, molecule.inchiKey)
      case "inchi"    => ok(`chemical/x-inchi`, molecule.inchi)
      case "mol"      => ok(`chemical/x-mdl-molfile`, molecule.structure)
      case "smi" | "smiles" => ok(`chemical/x-daylight-smiles`, molecule.formula)
      case _ => unsupported(format)
    }
  }

  def ok(media: WithFixedCharset, content: String): HttpResponse = HttpResponse(entity = HttpEntity(ContentType(media), content))

  def unsupported(format: String): HttpResponse =
    HttpResponse(entity = HttpEntity(s"unsupported format: $format"), status = StatusCodes.UnsupportedMediaType)

}