package com.wiley.wss.resolver.http

import akka.actor.ActorSystem
import akka.event.LoggingAdapter
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Sink
import com.wiley.wss.resolver.Application._
import com.wiley.wss.resolver.configs.{Configuration, Database}

import scala.concurrent.Future

class HttpService(implicit materializer: ActorMaterializer,
                           system: ActorSystem,
                           config: Configuration,
                           database: Database,
                           log: LoggingAdapter) extends Routes {

  private val serverSource = Http().bind(interface = config.httpHost, port = config.httpPort)

  val bindingFuture: Future[Http.ServerBinding] =
    serverSource.to(Sink.foreach {
      connection => connection.handleWith(routes)
    }).run()

}