package com.wiley.wss.resolver.http.api

import akka.Done
import akka.actor.{ActorSystem, Props}
import akka.event.LoggingAdapter
import akka.http.scaladsl.coding.GzipDecompressor
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Framing, Source}
import akka.util.ByteString
import com.wiley.wss.resolver.models.MoleculeEntity
import com.wiley.wss.resolver.services.{MoleculesService, PostProcessor}

import scala.concurrent.{ExecutionContext, Future}

class UploadApi(val moleculesService: MoleculesService)
               (implicit executionContext: ExecutionContext,
                         system: ActorSystem,
                         materializer: ActorMaterializer,
                         log: LoggingAdapter) {

  lazy val postProcessor = system.actorOf(Props[PostProcessor])

  val moleculeStructureKey: String = "Molecule"
  val frameDelimiter = Framing.delimiter(ByteString("$$$$"), Int.MaxValue, allowTruncation = true)
  val recordDelimiter: String = "\\n>[\\ ]{1,2}<"
  val keyDelimiter: String = ">"
  val decompressor = new GzipDecompressor()


  private val mapping = Map[String, String](
    "Molecule"          -> "structure",

    // NCI-Open_2012-05-01.sdf.gz
    // smiles is missing in NCI-Open database
    "Formula"           -> "formula",
    "Standard InChI"    -> "inchi",
    "Standard InChIKey" -> "inchikey",
    "Molecular Weight"  -> "weight",
    "DTP names"         -> "names",

    // FDA_SPL_Feb_2015.sdf.gz
    // inchi is missing
    "UNII_ANNOTATION_MF"       -> "formula",
    "UNII_ANNOTATION_INCHIKEY" -> "inchikey",
    "UNII_ANNOTATION_SMILES"   -> "smiles",
    "E_NAMESET"                -> "names",

    // SAVI_Rel1alpha_2015-07-14.sdf.gz
    "E_INCHI"    -> "inchi",
    "E_INCHIKEY" -> "inchikey",
    "SMILES"     -> "smiles",
    "E_WEIGHT"   -> "weight",
    "E_FORMULA"  -> "formula"
  )

  private def processRequest(source: Source[ByteString, Any]): Future[HttpResponse] = {
    val result = source.named("upload")
      .via(decompressor)
      .via(frameDelimiter)
      .map(_.utf8String)
      .map(_.trim())
      .filter(!_.isEmpty)
      .map(entry => {
        entry.split(recordDelimiter).map(line => {
          val fields = line.split(keyDelimiter, 2)
          fields.length match {
            case 1 => (moleculeStructureKey, io.Source.fromString(fields(0).trim()).getLines().mkString("\n"))
            case 2 => (fields(0), fields(1).trim())
            case _ =>
              log.error("Wrong line: |||" + line + "|||")
              ("None", "None")
          }
        }).toMap[String, String]
      })
      .filterNot(_.getOrElse("Structure Evaluation", "").startsWith("Inconsistent with Molecular Formula"))
      .map(_.map((entry: (String, String)) => (mapping.getOrElse(entry._1, entry._1), entry._2)))
      .map(entry => {
        val molecule = MoleculeEntity(
          structure = entry.getOrElse("structure", ""),
          formula   = entry.getOrElse("formula", ""),
          smiles    = entry.getOrElse("smiles", ""),
          inchi     = entry.getOrElse("inchi", ""),
          inchiKey  = entry.getOrElse("inchikey", ""),
          weight    = entry.getOrElse("weight", "-1").toDouble
        )

        val synonyms = entry.getOrElse("names", "")
          .split("\n").map(_.trim()).filter(!_.isEmpty).distinct.toSeq

        (molecule, synonyms)
      })
      .runForeach({
        case (molecule, synonyms) => moleculesService.addMolecule(molecule, synonyms)
      })

    result.map {
      case Done => HttpResponse(200)
      case _ => sys.error("Error")
    }
  }

  val route: Route =
    (path("upload") & post & extractRequest) {
      request => complete(processRequest(request.entity.withoutSizeLimit().dataBytes))
    }
}
