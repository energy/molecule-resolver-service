package com.wiley.wss.resolver.services

import akka.actor.Actor

case class Render(molecule: String)

class Renderer extends Actor {

  private val env = Array(
    "PATH=/bin:/usr/bin:/sbin:/usr/sbin/:/opt/marvinbeans/bin",
    "JAVA_HOME=/etc/java-config-2/current-system-vm")

  private val runtime = Runtime.getRuntime
  private val script = "/opt/marvinbeans/bin/molconvert"

  override def receive: Receive = {
    case Render(moleculePath) =>
      val cmd = Array(script, "png:w600,Q95,#FFFFFF", moleculePath, "-o", moleculePath + ".png")
      runtime.exec(cmd, env).waitFor()
  }
}
