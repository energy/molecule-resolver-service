package com.wiley.wss.resolver.services

import java.io.File
import java.nio.file.{Files, Paths}

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.event.LoggingAdapter
import akka.routing.RoundRobinPool
import com.wiley.wss.resolver.configs.Database
import com.wiley.wss.resolver.models.db.{MoleculeEntityTable, SynonymEntityTable}
import com.wiley.wss.resolver.models.{MoleculeEntity, SynonymEntity}

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.{Failure, Success, Try}


class MoleculesService(implicit val database: Database, executionContext: ExecutionContext, log: LoggingAdapter, system: ActorSystem)
  extends MoleculeEntityTable with SynonymEntityTable {

  import database._
  import driver.api._

  def findMolecule(id: Long): Future[Option[MoleculeEntity]] = {
    db.run(molecules.filter(_.id === id).result.headOption)
  }

  def findMoleculeByInchiKey(inchiKey: String): Future[Option[MoleculeEntity]] = {
    db.run(molecules.filter(_.inchiKey === inchiKey).result.headOption)
  }

  def findSynonyms(molecule: MoleculeEntity): Future[Seq[SynonymEntity]] = {
    db.run(synonyms.filter(_.moleculeId === molecule.id).sortBy(_.name).result)
  }

  def findMoleculeWithSynonyms(name: String): (MoleculeEntity, Array[String]) = {
    val mol   = Await.result(findExactMolecule(name), Duration.Inf)
    val names = Await.result(findSynonyms(mol.get._1), Duration.Inf)

    (mol.get._1, names.map(_.name).toArray)
  }

  def getDatabase(): Future[Map[String, Int]] = {
    val lengthSynonyms:  Future[Int] = db.run(synonyms.length.result)
    val lengthMolecules: Future[Int] = db.run(molecules.length.result)
    for {
      count1 <- lengthSynonyms
      count2 <- lengthMolecules
    } yield Map("synonyms" -> count1, "molecules" -> count2)
  }

  def findExactMolecule(query: String) = {
    db.run(molecules.join(synonyms).on(_.id === _.moleculeId).filter(m =>
      m._2.name.toLowerCase === query.toLowerCase ||
      m._1.inchi    === query ||
      m._1.inchiKey === query ||
      m._1.formula  === query
    ).result.headOption)
  }

  def findSynonyms(query: String, limit: Int) = {
    db.run(synonyms.filter(_.name.toLowerCase like '%' + query.toLowerCase + '%')
      .sortBy(_.name).map(_.name).take(limit.min(25).max(1)).to[List].result)
  }

  def findMolecule(name: String) = { //: Future[Option[MoleculeEntity]] = {
    db.run(molecules.join(synonyms).on(_.id === _.moleculeId).filter(_._2.name.toLowerCase like '%' + name + '%').result.headOption)
    //db.run(synonyms.filter(_.name like name).joinRight(_.).result.headOption)
  }

  def addSynonyms(moleculeId: Option[Long], names: Seq[String]): Int = {
    val sum: Int = names
      .map(name => SynonymEntity(moleculeId = moleculeId, name = name))
      .map(entity => db.run(synonyms.insertOrUpdate(entity)))
      .map(t =>
        Try(Await.result(t, Duration.Inf)) match {
          case Success(i) => i
          case Failure(e) =>
            println(e)
            0
        }).sum
    //println("Inserted " + sum + " synonyms")
    sum
  }

  def addMolecule(moleculeEntity: MoleculeEntity, names: Seq[String]):Unit = {
    val future: Future[Option[Long]] = db.run((molecules returning molecules.map(_.id)) += moleculeEntity)

    Try(Await.result(future, Duration.Inf)) match {
      case Success(moleculeId) =>
        log.info("Adding synonyms: " + names)
        addSynonyms(moleculeId, names)/* match {
          case Success(number) => log.info("successfully inserted {} of synonyms", number)
          case Failure(e)      => log.error(e, "failed to insert synonyms")
        }*/
      case Failure(e) =>
        log.error(e, "Cannot add molecule: " + moleculeEntity)
        val found = Await.result(findMoleculeByInchiKey(moleculeEntity.inchiKey), Duration.Inf)
        mergeMolecules(found.get, moleculeEntity, names);
    }
  }

  val dir = new File("conflicts")
  dir.mkdir()

  val rendererActor: ActorRef = system.actorOf(Props[Renderer]
    .withRouter(new RoundRobinPool(3)), "renderer")

  def mergeMolecules(existing: MoleculeEntity, moleculeEntity: MoleculeEntity, names: Seq[String]): Unit = {
    val existingStructure: String = existing.structure.lines.drop(2).mkString("\n")
    val moleculeStructure: String = moleculeEntity.structure.lines.drop(2).mkString("\n")

    val similar = existingStructure == moleculeStructure
    val path = if (similar) {
      "/similar"
    } else {
      "/different"
    }

    val conflictDirectory = new File(dir.getAbsolutePath + path, existing.id.get.toString)
    conflictDirectory.mkdirs()

    val existingDir = new File(conflictDirectory.getAbsolutePath, "existing.mol")
    val conflictDir = new File(conflictDirectory.getAbsolutePath, "conflict.mol")
    Files.write(Paths.get(existingDir.getAbsolutePath), existing.structure.getBytes)
    Files.write(Paths.get(conflictDir.getAbsolutePath), moleculeEntity.structure.getBytes)

    if (!similar) {
      // rendererActor ! Render(existingDir.getAbsolutePath)
       // rendererActor ! Render(conflictDir.getAbsolutePath)
    }

    log.info("Merging synonyms: {} into {}", names, existing)
    addSynonyms(existing.id, names)/* match {
      case Success(number) => log.info("Successfully inserted {} of synonyms", number)
      case Failure(e) => log.error(e, "Failed to insert synonyms")
    }*/

    // todo: compare with the existence entity and associate new synonyms if it's the same

    //log.error("failed to add molecule {}", found.get.id)
    //log.error(moleculeEntity.toString)
    //log.error("Exists:" + found.toString)
    //log.error("Names: " + names)
  }
}