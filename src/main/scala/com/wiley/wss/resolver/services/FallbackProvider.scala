package com.wiley.wss.resolver.services

import java.net.{URI, URLEncoder}

import com.wiley.wss.resolver.configs.Configuration
import com.wiley.wss.resolver.models.MoleculeEntity

import scala.collection.parallel.immutable.ParMap
import scala.concurrent.ExecutionContext
import scala.io.Source
import scala.util.Try

class FallbackProvider(implicit executionContext: ExecutionContext,
                                config: Configuration) {

  private val fields = List(
    "stdinchikey",  // Standard InChIKey
    "stdinchi",     // Standard InChI2
    "smiles",       // SMILES
    "mw",           // Molecular Weight
    "sdf",          // SD File
    "names",        // Names
    "formula"       // Chemical Formula
  )

  def download(identifier: String): ParMap[String, Option[String]] = {
    val encodedIdentifier = URLEncoder.encode(identifier, "UTF-8").replaceAll("\\+", "%20")
    fields.par.map(field => {
      val uri = URI.create(s"https://cactus.nci.nih.gov/chemical/structure/$encodedIdentifier/$field")
      val data = Try(Source.fromInputStream(uri.toURL.openStream()).mkString)
      (field, data.toOption)
    }).toMap
  }

  def fetchMolecule(identifier: String): (MoleculeEntity, Seq[String]) = {
    val data = download(identifier)
    val molecule = MoleculeEntity(
      structure = data.getOrElse("sdf", None).getOrElse(""),
      formula   = data.getOrElse("formula", None).getOrElse(""),
      smiles    = data.getOrElse("smiles", None).getOrElse(""),
      inchi     = data.getOrElse("stdinchi", None).getOrElse(""),
      inchiKey  = data.getOrElse("stdinchikey", None).getOrElse(""),
      weight    = data.getOrElse("mw", None).getOrElse("-1").toDouble
    )
    val synonyms = data.getOrElse("names", None).getOrElse("").split("\n").map(_.trim()).filter(!_.isEmpty).distinct.toSeq
    (molecule, synonyms)
  }
}