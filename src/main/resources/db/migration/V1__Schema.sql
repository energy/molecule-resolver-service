create table "molecules" (
  "id"        bigserial primary key,
  "structure" text        not null,
  "formula"   text        not null,
  "smiles"    text        not null,
  "inchikey"  varchar(50) not null unique,
  "inchi"     text        not null,
  "weight"    double precision
);

create table "synonyms" (
  "id"          bigserial primary key,
  "molecule_id" bigserial,
  "name"        text not null unique
);

alter table "synonyms" add constraint "molecule_fk"
 foreign key ("molecule_id") references "molecules" ("id")
 on update no action on delete cascade;
